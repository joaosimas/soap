#language: pt
Funcionalidade: XML
	TESTE XML

@teste
Esquema do Cenario: WSDL EM POST COMO SE FOSSE REST
    
 	Dado que seja feita uma requisição em um WSDL
  	Quando os parametros forem enviados com o método post e text xml
    | logradouro     		| <logradouro>   		|
    | localidade  			| <localidade>		 	|
    | UF					| <UF>					|
	Então o <status_code> precisa retornar 200
    Exemplos:
    |logradouro   |localidade     |UF		   |status_code |
    |	    	  |		          |			   |200   	    |